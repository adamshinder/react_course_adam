//we'll be using prisma-binding - JS binding to the Prisma database
// This file connects to the remote prisma DB and gives us the ability to query it with JS
const { Prisma } = require('prisma-binding');

console.log(process.env.PRISMA_ENDPOINT);
const db = new Prisma({
  typeDefs: 'src/generated/prisma.graphql',
  endpoint: process.env.PRISMA_ENDPOINT,
  secret: process.env.PRISMA_SECRET,
  debug: true, //true console.log the errors
});

module.exports = db; //exports this database to use other places