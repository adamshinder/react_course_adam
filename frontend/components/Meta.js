import Head from 'next/head';// a custom head from next.js

const Meta = () => (
  <Head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charSet="utf-8" />
    <link rel="shortcut icon" href="/static/favicon.png" />
    <link rel="stylesheet" type="text/css" href="/static/nprogress.css" /> 
    <title>Sick Fits!</title>
  </Head>
);
export default Meta;
//we're using empty state functions because almost all state will be handled by other high order components, these are just display
//nprogress is a cool loading bar
//current title is Sick Fits! but that will change later