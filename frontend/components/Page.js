import React, { Component } from "react";
import Header from "../components/Header";
import Meta from "../components/Meta";
import styled, { ThemeProvider, injectGlobal } from "styled-components";
//import _document from "../pages/_document.js";

//set typrography globably
//CSS stands for Cascading Style Sheets
//nice to have it in JS because we can have global formmating and all other styling will be specific
const StyledPage = styled.div`
  background: white;
  color: black;
`;
const theme = {
  red: "#FF0000",
  black: "#393939",
  grey: "#3A3A3A",
  lightgrey: "#E1E1E1",
  offWhite: "#EDEDED",
  maxWidth: "1000px",
  bs: "0 12px 24px 0 rgba(0, 0, 0, 0.09)",
};

const Inner = styled.div`
  max-width: ${(props) => props.theme.maxWidth};
  margin: 0 auto;
  padding: 2rem;
`;

//the format to initate and set functions styling called a selector--> injectGlobal ``;
injectGlobal`
  @font-face { //incorporates a font into the file
    font-family: 'radnika_next';
    src: url('/static/radnikanext-medium-webfont.woff2') format('woff2');
    font-weight: normal;
    font-style: normal;
  }
  html {
    box-sizing: border-box; //making the base font size 10
    font-size: 10px;
  }
  *, *:before, *:after {
    box-sizing: inherit; //inherit what was set in html, currently border-box
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem; //base font size will be 10 so 1.5 is 15 pixels
    line-height: 2;
    font-family: 'radnika_next';
  }
  a {
    text-decoration: none;
    color: ${theme.black};
  }
  //button {  font-family: 'radnika_next'; }
`;
class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <StyledPage>
          <Meta />
          <Header />
          <Inner>{this.props.children}</Inner>
        </StyledPage>
      </ThemeProvider>
    );
  }
}
export default Page;

/*
const MyButton = styled.button`
  background: red;
  font-size: ${(props) =>
    props.huge
      ? `160px`
      : `50px`}; //JS saying if the props of button is huge, make it 200px otherwise make it 50px
  span {
    font-size: 100px;
  }
`; //these back ticks are called a tag template literal where there's a function that is basically a string that takes in design


const MyButton = styled.button`
  background: red;
  font-size: 70px;
  .shalom {
    font-size: 100px;
  }
`; 
// then could do 

        <MyButton>
          Click Me <span classname = "shalom"> Shalom </span>
        </MyButton>

        This would also work


class Page extends Component {
  render() {
    return (
      <div>
        <Meta />
        <Header />
        <MyButton huge>
          Click Me <span> Shalom </span>
        </MyButton>
        {this.props.children}
      </div>
    );
  }
}
export default Page;
*/
