import App, { Container } from 'next/app'; //keeps states between pages
import Page from '../components/Page';
class myApp extends App {
  render() {
    const { Component } = this.props;

    return (
      <Container>
         <Page>
        <Component />
        </Page>
      </Container>
    );
  }
  }

export default myApp;
//component should be sell or index
