const { GraphQLServer } = require('graphql-yoga'); // import the server
const Mutation = require('./resolvers/Mutation'); // pulls the Mutation resolver
const Query = require('./resolvers/Query'); //pulls the Query resolver
const db = require('./db'); //pulls in the database

// Create the GraphQL Yoga Server

function createServer() {
  return new GraphQLServer({
    typeDefs: 'src/schema.graphql',
    resolvers: {
      Mutation,
      Query,
    },
    resolverValidationOptions: {
      requireResolversForResolveType: false,
    },
    context: req => ({ ...req, db }),
  }); //this context lets you access data from resolvers
}

module.exports = createServer;