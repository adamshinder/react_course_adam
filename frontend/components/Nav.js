import Link from "next/link";
import NavStyles from "./styles/NavStyles";
//when clicked, it just changes the url, doesn't actully cange the page, it's a one page project
//pages are built on demand the first time, cache remebers them for the next time
const Nav = () => (
  <NavStyles>
    <Link href="/sell">
      <a> Sell </a>
    </Link>
    <Link href="/">
      <a> Home </a>
    </Link>
    <Link href="/signup">
      <a> Sign Up </a>
    </Link>
    <Link href="/orders">
      <a> Orders </a>
    </Link>
    <Link href="/me">
      <a> Account </a>
    </Link>
    <Link href="/sell">
      <a> Sell </a>
    </Link>
  </NavStyles>
);
export default Nav;
